//Sandbox

//package vokabeltrainer;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class Sandbox extends Frame implements Serializable
{
	
	
	
	public Sandbox()
	{
		setLayout(new BorderLayout());
		setSize(200,200);
		setBackground(Color.GREY);
		add(new Button("oben"),BorderLayout.NORTH);
		add(new Button("rechts"),BorderLayout.EAST);
		add(new Button("unten"),BorderLayout.SOUTH);
		add(new Button("links"),BorderLayout.WEST);
		//add(new Button("mitte"),Border.CENTER);
		addWindowListener(new WiL());
		setVisible(true);
	}
	
	
	
	
	
	private class WiL extends WindowAdapter//implements WindowsListener = actionPerformed
	{
		@Override
		public void windowClosing(WindowEvent we)
		{
			setVisible(false);
			dispose();
			System.exit(0);
		}
	}
	
	public static void main(String[]arguments)
	{
		new Sandbox();
	}
}
