/*
 * R.Schneider
 * Projekt Vokabeltrainer
 * 23.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * User
 */

import java.io.*;
import java.util.*;
//package vokabeltrainer


class Model
{
	//Verbindung zum Controller
	private final Controller controller;

	private TreeSet<User>tsUser;
	
	private Dateioperationen files;
	
	//--------------------------
	
	public Model(Controller controller)
	{
		this.controller=controller;
		
		//
		files=new Dateioperationen();
		//Treeset mit Usern erstellen
		tsUser=new TreeSet<User>();
	}
	
	//--------------------------
	
	//----getter----------------
	
	//Zeigt die Zahl der User an
	public int getUserCount()
	{
		return tsUser.size();
	}
	
	//Zeigt alle User an
	public String getAllUser()
	{
		String rueckgabe="";

		for(User user:tsUser)
		{
			rueckgabe+=user+" ";			//	(User)user
		}
		return rueckgabe;
	}
	




	//-----Methoden-------------
	
	public void boomerang(String boomerang)
	{
		if(null!=boomerang)
		{
			controller.setStatuszeile(boomerang.trim());
		}
	}

	public TreeSet<User> ladeAlleUserAusDatei()
	{
		User ren=new User("Ren","Radier");
		//ladeDatei
		tsUser.add(ren);
		return tsUser;
	}

	public void ladeAlleUserInModel()
	{
		tsUser.addAll(null);
	}
	
	public void ladeAlleTabellenVonUser(User user)
	{
		
	}



	public String buchstabensalat(String original)
	{
		//Temp. Speicher
		String rueckgabe=original;

		//Random

		if(null!=original)
		{
			for(int i=0;i<original.length();i++) //*2
			{
				rueckgabe=rueckgabe.replace("O","y");
				//an stelle rueckgabe.substring(rd.nextInt(original.length(),1));
				//
			}
		}
		System.out.println(original.length());
		System.out.println(rueckgabe.substring(1,1));

		return rueckgabe;
	}


	//adder
	public boolean addUser(User user)
	{
		//Auf NULL pr�fen.
		controller.setStatuszeile(user.getGivenname()+" "+user.getSurname());
		return (null!=user)?tsUser.add(user):false;
	}
	
	//Alle User speichern
	public boolean saveAllUser()
	{
		files.schreibeObjektdatei(tsUser,"c:/temp/u.ser");
		return true;
	}
	
	//TESTBEREICH
	
	public User erstelleUser(String givenname, String surname)//(String givenname, String surname)
	{
		if((null!=givenname)&&(null!=surname))
		{
			return new User(givenname.trim(),surname.trim());
		}
		else
		{
			return new User("Maximilian","Mustermann");
		}
	}
	
	public String erstelleTabelle()
	{
		Tabelle tabl=new Tabelle("tabelleName");
		
		return tabl+" ";
	}
	
	//TESTBEREICH
	
	//hashCode equals , compare to



	//-------Overrides-------------
	
	@Override
	public String toString()
	{
		return "Model";
	}
}
