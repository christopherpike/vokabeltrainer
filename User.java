/*
 * R.Schneider
 * Projekt Vokabeltrainer
 * 23.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * User
 */

import java.io.*;
import java.util.*;
//package vokabeltrainer


class User implements Comparable,Serializable
{
	private final String givenname;
	
	private final String surname;
	
	private TreeSet<Tabelle>tabellen=new TreeSet<Tabelle>();
	
	private ArrayList<Object>y=new ArrayList<Object>();
	
	//--------------------------
	
	public User(String givenname, String surname)
	{
		this.givenname=(null!=givenname)?givenname.toLowerCase():"Maximilan";
		this.surname=(null!=surname)?surname.toLowerCase():"Mustermann";
	}
	
	//--------------------------
	
	//----getter----------------
	
	public String getGivenname()
	{
		return givenname;
	}
	
	public String getSurname()
	{
		return surname;
	}
	
	public Tabelle getTabelle(String tabellenname)
	{
		return (Tabelle)tabellen.first();
	}

	public boolean removeTabelle(String tabellenname)
	{
		return tabellen.remove(null);
	}

	public void clearTabellen()
	{
		tabellen.clear();
	}

	public boolean addTabelle(Tabelle tabelle)
	{
		return tabellen.add(tabelle);
	}



	public int getDatabase()
	{
		return tabellen.size();
	}
	
	//hashCode überschreiben
	@Override
	public int hashCode()
	{
		return (givenname.hashCode()+surname.hashCode());
	}

	//equals
	//@Override
	//public boolean equals()
	//{
	//}
	
	//CompareTo
	@Override
	public int compareTo(Object o)
	{
		

		
		return 1;
	}
	
	
	@Override
	public String toString()
	{
		return givenname+" "+surname;
	}
}
