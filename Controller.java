/*
 * R.Schneider
 * Projekt Vokabeltrainer
 * 22.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Controller
 */

import java.io.*;
import java.awt.*;
import java.awt.event.*;

//import javax.swing.*;

//package vokabeltrainer


class Controller
{
	private final Model model;
	
	private final View view;
	
	//private JD jdialog;
	
	private ControllerListener controllerListener;
	
	//--------------------------
	
	public Controller()
	{
		//Null
		model=new Model(this);//this
		controllerListener=new ControllerListener();
		view=new View(controllerListener);
		//----------------------------
		//jdialog=new JD();
	}
	
	//--------------------------
	
	//----getter----------------
	
	
	private class ControllerListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{


			if(ae.getActionCommand().equals("User speichern"))
			{
				System.out.println(view.getMenuBar().getMenuCount()+" ");
				
				//System.out.println(view.getAccessibleContext().getAccessibleChildrenCount());
				
										//Inhalt des Frames     //Panel                 //TextField
				//System.out.println(view.getAccessibleContext().getAccessibleChild(0).getAccessibleContext().getAccessibleChildrenCount());//.getComponentCount());//getComponent(6).getText();

				model.boomerang(view.getUserHinzufuegenGivenname());

				//model.boomerang( ((JTextField)((Container)jdialog.getContentPane()).getComponent(0)).getText() );

				//String givenname=view.getConponent; String surname="Nachname";
				//view.getFrame().getComponente().gettxt.getText();
				model.addUser(model.erstelleUser(view.getUserHinzufuegenGivenname(),view.getUserHinzufuegenSurname()));
				
			}

			if(ae.getActionCommand().equals("User laden"))
			{
				model.boomerang("Jupp?");
				
			}

			String temp=ae.getActionCommand();
			switch(temp)
			{
				/*
				case"Programm speichern":
				
				case"Programm beenden":
					
				case"User anlegen":
				
				case"User ausw�hlen":
				
				case"User l�schen":
					
				//---------------------------------
				case"Tabelle anlegen":
					
				case"Tabelle ausw�hlen":
				
				case"Tabelle l�schen":
					
				//-----------------------------------
				case"Vokabel hinzuf�gen":
					
				case"Vokabel bearbeiten":
				
				case"Vokabel l�schen":
				
				//-------------------------------------
				case"Lern-Modus Karteikarten":
				
				case"Lern-Modus JustKlick":
					
				case"Lern-Modus Flip-Reverse":
				
				default:

				*/


				case"User laden":
					{
						model.boomerang("Warum?");
						break;
					}
				case"User speichern":
					{

						model.boomerang("Warum?");
						break;
					}
				case"User":
					{

						model.boomerang("Warum?");
						break;
					}
				case"User s":
					{

						model.boomerang("Warum?");
						break;
					}
				case"User sp":
					{
						
					}



				default:
					{
						System.out.println("Na");
					}
			}
			
		}
	}
	
	public void setStatuszeile(String meldung)
	{
		if(null!=meldung)
		{
			view.setStatuszeile(meldung.trim());
		}
		else
		{
			view.setStatuszeile("Fehler");
		}
	}
	
	//-----------Overrides--------------------------
	
	@Override
	public String toString()
	{
		return "Controller";
	}
	
	public static void main(String[]arguments)
	{
		Controller con=new Controller();
	}
}
