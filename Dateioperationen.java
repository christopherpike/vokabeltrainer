/*
 * R.Schneider
 * ioStream
 * 12.02.2018
 * Dateioperationen
 */

import java.io.*;
import java.util.*;

class Dateioperationen
{
	private File savegame=new File("savegame.ser");
	
	//private File savetext=new File("frageantwort.txt");
	
	private File savetext=new File("c:/temp/savegameText.txt");
	
	private File saveSpielerObjektTreeSet=new File("c:/temp/savespielerobjektetreeset.ser");
	
	//-------------
	
	
	
	
	
	
	//------------
	
	//Erzeuge Textdatei, wenn noch nicht auf dem Festspeicher

	
	//Erzeuge Objektdatei, wenn noch nicht auf dem Festspeicher
	public boolean erzeugeObjektdatei(String dateiname)
	{
		boolean rueckgabe=false;
		
		//savespielerobjektetreeset.ser
		
		//Pr�fe ob Argument gef�llt ist
		if(null!=dateiname)
		{
			//Erzeuge Referenz auf m�gliche Datei
			File savedatei=new File(dateiname.toLowerCase().trim());

			//Pr�fe ob Dateiname existiert
			if(savedatei.exists())
			{
				//Pr�fe ob Datei ein File oder ein Directory ist
				try
				{
					if(!savedatei.isFile())
					{
						if(savedatei.isDirectory())
						{
							rueckgabe=false;
						}
					}
					else
					{
						if(savedatei.isFile())
						{
							rueckgabe=true;
						}
					}
				}
				catch(Exception ext) //IOException ext
				{
					System.out.println(ext+" Fehler beim Pr�fen von Datei, ob File oder Directory.");
				}

			}
			else
			{
				try
				{
					rueckgabe=savedatei.createNewFile();
					
				}
				catch(IOException exc)
				{
					System.out.println(exc+" Fehler: Datei konnte nicht erzeugt werden.");
				}
			}
		}
		
		return rueckgabe;
	}
	

	
	//Lese Textdatei f�r Fragen und Antworten
	public TreeSet<String>leseTextdatei()//static
	{
		TreeSet<String>textzeile=new TreeSet<String>();
		
		FileReader textdateiReader=null;
		
		BufferedReader textReader=null;
		
		if(savetext!=null)
		{
			try
			{
				//
				textdateiReader=new FileReader(savetext);
				//
				textReader=new BufferedReader(textdateiReader);
				
				String temp_textzeile="";
				
				while(  (temp_textzeile=textReader.readLine())!=null)
				{
					//System.out.println();
					textzeile.add(temp_textzeile);
				}
			}
			catch(IOException exc)
			{
				System.out.println(exc);
			}
			finally
			{
				if(textReader!=null)
				{
					try
					{
						textReader.close();
					}
					catch(IOException exe)
					{
						System.out.println(exe);
					}
				}
				if(textdateiReader!=null)
				{
					try
					{
						textdateiReader.close();
					}
					catch(IOException exe)
					{
						System.out.println(exe);
					}
				}
			}
		}

		return textzeile;
	}
	
	
	//Lese Objektdatei
	public Object leseObjektdatei()
	{
		Object rueckgabeObjekt=null;

		if(null!=saveSpielerObjektTreeSet)
		{
			if(saveSpielerObjektTreeSet.exists())
			{
				FileInputStream streamSaveSOTS=null;
				
				ObjectInputStream objectStreamSaveSOTS=null;

				try
				{
					//FileInputStream �ffnen
					streamSaveSOTS=new FileInputStream(saveSpielerObjektTreeSet);

					//ObjectInputStream �ffnen
					objectStreamSaveSOTS=new ObjectInputStream(streamSaveSOTS);
					
					try
					{
						rueckgabeObjekt=objectStreamSaveSOTS.readObject();
						
					}
					catch(ClassCastException falscheKlasse)
					{
						System.out.println(falscheKlasse);
					}
					catch(ClassNotFoundException fehlendeKlasse)
					{
						System.out.println(fehlendeKlasse);
					}
				}
				catch(IOException ex)
				{
					System.out.println(ex);//
				}
				finally
				{
					if(null!=objectStreamSaveSOTS)
					{
						try
						{
							objectStreamSaveSOTS.close();
						}
						catch(IOException exc)
						{
							System.out.println(exc+" Beim Schlie�en des ObjectStreams.");
						}
					}
					if(null!=streamSaveSOTS)
					{
						try
						{
							streamSaveSOTS.close();
						}
						catch(IOException exs)
						{
							System.out.println(exs+" Beim Schlie�en des InputStreams.");
						}
					}
					
				}
				
			}
		}

		//Inputsream in object stream wickeln
		
		return rueckgabeObjekt;
	}
	
	/*
	//Lese Objektdatei
	public TreeSet<Object> leseSpielerdatei()
	{
		//	
		TreeSet<Object>rueckgabeObjekt=null;		//?Hier der Fehler?

		if(null!=saveSpielerObjektTreeSet)
		{
			if(saveSpielerObjektTreeSet.exists())
			{
				FileInputStream streamSaveSOTS=null;
				
				ObjectInputStream objectStreamSaveSOTS=null;

				try
				{
					//FileInputStream �ffnen
					streamSaveSOTS=new FileInputStream(saveSpielerObjektTreeSet);

					//ObjectInputStream �ffnen
					objectStreamSaveSOTS=new ObjectInputStream(streamSaveSOTS);
					
					try
					{
						//
						rueckgabeObjekt=(TreeSet<Object>)objectStreamSaveSOTS.readObject();//(TreeSet<Object>)
						
					}
					catch(ClassCastException falscheKlasse)
					{
						System.out.println(falscheKlasse);
					}
					catch(ClassNotFoundException fehlendeKlasse)
					{
						System.out.println(fehlendeKlasse);
					}
				}
				catch(IOException ex)
				{
					System.out.println(ex);//
				}
				finally
				{
					if(null!=objectStreamSaveSOTS)
					{
						try
						{
							objectStreamSaveSOTS.close();
						}
						catch(IOException exc)
						{
							System.out.println(exc+" Beim Schlie�en des ObjectStreams.");
						}
					}
					if(null!=streamSaveSOTS)
					{
						try
						{
							streamSaveSOTS.close();
						}
						catch(IOException exs)
						{
							System.out.println(exs+" Beim Schlie�en des InputStreams.");
						}
					}
					
				}
				
			}
		}

		//Inputsream in object stream wickeln
		
		return rueckgabeObjekt;
	}

	*/

	//Lese Objektdatei f�r Quiz
		public Object leseQuizdatei()
	{
		return "q";
	}

	//Schreibe Objektdatei
	public boolean schreibeObjektdatei(Object obj,String dateiname)
	{
		boolean rueckgabe=false;
		
		File savedatei=new File(dateiname);

		if(null!=obj)
		{
			
			if(erzeugeObjektdatei(dateiname))
			{

				FileOutputStream diskdrive=null;

				ObjectOutputStream saveObjectSpielerObjektTreeSet=null;

				try
				{
					//Verpacke
					diskdrive=new FileOutputStream(savedatei);

					saveObjectSpielerObjektTreeSet=new ObjectOutputStream(diskdrive);


					// write
					saveObjectSpielerObjektTreeSet.writeObject(obj);

					// flush
					saveObjectSpielerObjektTreeSet.flush();
					
					/*
					
					
					FileOutputStream fos = new FileOutputStream("t.tmp");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeInt(12345);
        oos.writeObject("Today");
        oos.writeObject(new Date());


					*/
				}
				catch(Exception e)
				{

				}
				finally
				{
					if(null!=saveObjectSpielerObjektTreeSet)
					{
						try
						{
							saveObjectSpielerObjektTreeSet.close();
						}
						catch(Exception e)
						{
							System.out.println(e+" Fehler beim Schlie�en des ObjectStreams.");
						}
					}
					if(null!=diskdrive)
					{
						try
						{
							diskdrive.close();
						}
						catch(Exception e)
						{
							System.out.println(e+" Fehler beim Schlie�en des ObjectStreams.");
						}
					}

				}
			}
			
		}
		return rueckgabe;
	}

	public String setUp()
	{
		return "Hurra!";
	}
	
	
	//Schreibe speichere Textdatei f�r Fragen und Antworten
}
