/*
 * R.Schneider
 * Projekt Vokabeltrainer
 * 23.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/vokabeltrainer.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * View
 */

import java.io.*;
import java.awt.*;
import java.awt.event.*;
//package vokabeltrainer


class View extends Frame
{
	//private final String givenname;
	
	//private final String surname;
	
	//private int database;
	
	private Panel pnlUserHinzufuegen;
	
	private Panel pnlUserAuswahl;
	
	private Panel pnlUserLoeschen;
	
	private Panel pnlUserInformationen;
	
	//private Panel pnlUserInformationen;
	
	//private Panel pnlUserScore;
	
	private Panel pnlVokabelTabelleHinzufuegen;
	
	private Panel pnlVokabelTabelleAuswaehlen;
	
	private Panel pnlVokabelTabelleLoeschen;
	
	private Panel pnlVokabelHinzufuegen;
	
	private Panel pnlVokabelEditieren;
	
	private Panel pnlVokabelLoeschen;
	
	private Panel pnlSpielKarteikartenModus;
	
	private Panel pnlSpielBuchstabenModus;
	
	private Panel pnlSpielMultipleModus;
	
	//Statusleiste
	private Panel pnlStatusleiste;
	
	private List listUser;
	
	private Choice choiceUser;
	
	private ActionListener rActionListener;
	
	private ActionListener controllerListener;



	//private Panel pnlUserAuswahl;
		
	//--------------------------
	
	public View(ActionListener controllerListener)//(ControllerListener controllerListener)/
	{
		//Null
		this.controllerListener=controllerListener;
		listUser=new List(2,false);
		
		choiceUser=new Choice();
		
		rActionListener=new RActionListener();

		setMenuBar(startframe());
		//Fenster-Startgr��e
		setSize(600,400);
		//Fenster-Startpunkt
		setLocation(100,100);
		//Rahmen anzeigen
		setUndecorated(false);
		//Background
		setBackground(Color.LIGHT_GRAY);
  		//Layout
		//setLayout(new CardLayout());
		setLayout(new GridLayout());
		
		add(userHinzufuegen());
		//add(userAuswahl());
		//add(userLoeschen());
		//add(vokabelTabelleHinzufuegen());

		//add(vokabelTabelleAuswahl());
		//add(vokabelHinzufuegen());
		//add(vokabelnAuswahl());
		//add(vokabelEditieren());
		//add(vokabelnLoeschen());
		//add(spielKarteikartenModus());

		//add(spielBuchstabenModus());
		//add(spielMultipleModus());
		
		//addActionListener(new RActionListener());

		//add(statusleiste());//,BorderLayout.BOTTOM

		addWindowListener(new RWindowListener());
		//Anzeigen
		setVisible(true);
	}
	
	//--------------------------
	
	//----getter----------------
	
	//-----Methoden-----------
	
	public MenuBar startframe()
	{	
		//Erzeuge Menubar
		MenuBar menubar=new MenuBar();
		//Erzeuge Menu Datei
		Menu menuDatei=new Menu("Datei");
		//Erzeuge Menu User
		Menu menuUser=new Menu("User verwalten");
		//Erzeuge Menu Tabellen
		Menu menuTabellen=new Menu("Tabellen verwalten");
		//Erzeuge Menu Vokabeln verwalten
		Menu menuVokabeln=new Menu("Vokabeln verwalten");
		//Erzeuge Menu Lern-Modi
		Menu menuLernModi=new Menu("Lern-Modi");
		//---------------------------------------
		
		//MenuItem f�r Datei
		MenuItem mIProgrammBeenden=new MenuItem("Programm beenden");
		
		//---------------------------

		//MenuItem f�r User
		MenuItem mIUserAnlegen=new MenuItem("User anlegen");
		
		//MenuItem f�r User
		MenuItem mIUserAuswaehlen=new MenuItem("User ausw�hlen");
		
		//MenuItem f�r User
		MenuItem mIUserLoeschen=new MenuItem("User l�schen");
		//------------------------------------
		
		//MenuItem f�r Tabelle
		MenuItem mITabelleAnlegen=new MenuItem("Tabelle anlegen");
		
		
		//Vokabeln----------------------------------------------------
		//MenuItem f�r Vokabeln
		MenuItem mIVokabelnHinzufuegen=new MenuItem("Vokabel hinzuf�gen");

		//MenuItem f�r Vokabeln
		MenuItem mIVokabelnAuswahl=new MenuItem("Vokabel ausw�hlen");

		//MenuItem f�r Vokabeln
		MenuItem mIVokabelnEditieren=new MenuItem("Vokabel bearbeiten");

		//MenuItem f�r Vokabeln
		MenuItem mIVokabelnLoeschen=new MenuItem("Vokabel l�schen");
		
		//Lern-Modi--------------------------------------------------
		//MenuItem f�r Lern-Modi
		MenuItem mILernModiKartei=new MenuItem("Lern-Modus Karteikarten");
		
		//MenuItem f�r Lern-Modi
		MenuItem mILernModiCowGirl=new MenuItem("Lern-Modus JustKlick");
		
		//MenuItem f�r Lern-Modi
		MenuItem mILernModiCowGirlReverse=new MenuItem("Lern-Modus Flip-Reverse");
		
		
		
		//Adde Menu Items----------------
		menuDatei.add(mIProgrammBeenden);
		
		//ActionListener
		menuDatei.addActionListener(rActionListener);
		
		//Adde Menu Items-------------------
		menuUser.add(mIUserAnlegen);
		
		//Adde Menu Items
		menuUser.add(mIUserAuswaehlen);
		
		//Adde Menu Items
		menuUser.add(mIUserLoeschen);
		
		menuUser.addActionListener(rActionListener);
		
		
		//Adde Menu Items Vokabeln---------
		menuVokabeln.add(mIVokabelnHinzufuegen);
		
		//Adde Menu Items Vokabeln
		menuVokabeln.add(mIVokabelnAuswahl);
		
		//Adde Menu Items Vokabeln
		menuVokabeln.add(mIVokabelnEditieren);
		
		//Adde Menu Items Vokabeln
		menuVokabeln.add(mIVokabelnLoeschen);
		
		menuVokabeln.addActionListener(rActionListener);
		
		//-------------Tabellen--------

		menuTabellen.add(mITabelleAnlegen);
		
		menuTabellen.addActionListener(rActionListener);

		//----------
		
		//Adde Menu Items Lern-Modi---------
		menuLernModi.add(mILernModiKartei);

		//Adde Menu Items Lern-Modi
		menuLernModi.add(mILernModiCowGirl);

		//Adde Menu Items Lern-Modi
		menuLernModi.add(mILernModiCowGirlReverse);
		
		menuLernModi.addActionListener(rActionListener);

		//Adde Datei-Menu-----------------------------------------------------------
		menubar.add(menuDatei);
		//Adde User-Menu
		menubar.add(menuUser);
		//Adde Tabellen-Menu
		menubar.add(menuTabellen);
		//Adde Vokabel-Menu
		menubar.add(menuVokabeln);
		//Adde Lern-Modi-Menu
		menubar.add(menuLernModi);
		
		//Adde ActionListener------------------------------------

		//menubar.addActionListener(rActionListener);

		//-----------------------------
		
		return menubar;
	}
	
	//User hinzuf�gen
	public Panel userHinzufuegen()
	{
		//Erzeuge Panel um User hizuf�gen zu k�nnen
		pnlUserHinzufuegen=new Panel();

		//Layout
		//setLayout(new CardLayout());
		
		//Label User Hinzuf�gen was beachtet werden soll
		Label lblUserHinzufuegenHiweiss=new Label("Sie k�nnen hier einen neuen, nicht vorhanden Benutzer hinzuf�gen."
		
			+" Der User muss �ber einen Vornamen und einen Nachnamen verf�gen, und wird nur dann hinzugef�gt, wenn die"
			
			+" Kombination aus Vornamen und Nachnaem nciht schon existiert. Gro�/Kkeinschreibung wird nicht beachtet.");
		
		//Beschriftung Vorname
		Label lblGivenname=new Label("Vorname:");
		
		//Textfeld f�r den Vornamen
		TextField txtGivenname=new TextField("Maximilian",30);
		
		//Beschriftung Vorname
		Label lblSurname=new Label("Nachname:");
		
		//Textfeld f�r den Nachnamen
		TextField txtSurname=new TextField("Mustermann",30);
		
		//Button zum Speichern
		Button bntSave=new Button("User speichern");//on/off
		bntSave.addActionListener(rActionListener);
		bntSave.addActionListener(controllerListener);
		
		//Button zum Speichern
		Button bntCancel=new Button("Abbrechen");//on/off
		bntCancel.addActionListener(rActionListener);
		bntCancel.addActionListener(controllerListener);
		
		//adden
		pnlUserHinzufuegen.add(lblUserHinzufuegenHiweiss);
		pnlUserHinzufuegen.add(lblGivenname);
		pnlUserHinzufuegen.add(txtGivenname);
		pnlUserHinzufuegen.add(lblSurname);
		pnlUserHinzufuegen.add(txtSurname);
		pnlUserHinzufuegen.add(bntSave);
		pnlUserHinzufuegen.add(bntCancel);
		
		//---------------------------------------------------
		return pnlUserHinzufuegen;
	}
	
	//User w�hlen
	public Panel userAuswahl()
	{
		//Erzeuge UserAuswahl-Panel
		pnlUserAuswahl=new Panel();

		//Gridlayout f�r das Panel
		pnlUserAuswahl.setLayout(new GridLayout());

		//Versuch
		FileDialog fileDialog=new FileDialog(this,"Dann w�hle mal sch�n");
	
		//Beschriftung des Panels
		Label lblVerfuegbareUser=new Label("Hier sind alle verf�gbaren User gelistet.");

		//Liste f�llen zum Testen, sp�ter mit Methode .....
		listUser.add("Alois");
		listUser.add("Andrea");
		listUser.add("Alexandra");
		listUser.add("Steffi");
		listUser.add("Steffan");
		listUser.add("Frank");
		listUser.add("Fabiene");
		listUser.add("Francis");
		listUser.addActionListener(rActionListener);

		//Liste was gibt es noch?
		choiceUser.add("Albert");
		choiceUser.add("Aphrodithe");
		choiceUser.add("Bella");
		choiceUser.add("Elisabeth");//true
		
		
		//Beschriftung Vorname
		Label lblUserAusgewaehlt=new Label("User x ist ausgew�hlt");
		
		//Butto zum
	
	
		//
		//pnlUserAuswahl.add(fileDialog);
		pnlUserAuswahl.add(lblVerfuegbareUser);
		pnlUserAuswahl.add(listUser);
		pnlUserAuswahl.add(lblUserAusgewaehlt);
		pnlUserAuswahl.add(choiceUser);
		//pnlUserAuswahl.add(listUser);
	
		//------------------------------------
		return pnlUserAuswahl;
	}
	
	//Panel userAuswahl()
	public Panel userLoeschen()
	{
		//Erzeuge User L�schen Panel
		pnlUserLoeschen=new Panel();

		//Layout
		//setLayout(new CardLayout());

		//Beschriftung Panel User l�schen
		Label lblUserLoeschenHinweis=new Label("Hiermit k�nnen Sie den ausgew�hlten User l�schen");
		
		//Beschriftung
		Label lblUserWirdGeloescht=new Label("Der User x wird gel�scht, wenn sie auf User L�schen klicken!");
		
		//Button zum L�schen vom User
		Button bntUserLoeschen=new Button("Ausgew�hlten User l�schen");
		bntUserLoeschen.addActionListener(rActionListener);
		
		//Button zum Abbrechen
		Button bntUserAbbrechen=new Button("Abbrechen");
		bntUserAbbrechen.addActionListener(rActionListener);
		
		//Adde ---------------------------------------
		//Beschreibung genereller Hinwei�
		pnlUserLoeschen.add(listUser);
		
		//Liste mit ausgew�hltem User
		pnlUserLoeschen.add(listUser);

		//Beschreibung wer gel�scht wird
		pnlUserLoeschen.add(lblUserWirdGeloescht);
		
		//L�sch-Button
		pnlUserLoeschen.add(bntUserLoeschen);
		
		//Abbruch Button
		pnlUserLoeschen.add(bntUserAbbrechen);

		//------------------------------------
		return pnlUserLoeschen;
	}
	
	
	
	//
	//
	public Panel userInformationen()
	{
		Panel pnlUserAuswahl=new Panel();
		
		//Layout
		//setLayout(new CardLayout());

		//Username
		
		//User Tabellen
		
		//User Scorecard




		//------------------------------------
		return pnlUserAuswahl;
		
		
		
		
		
	}
	//
	//
	
	
	//Modi w�hlen
	
	//FileDialog
	//FileDialog(this, "Datei ausw�hlen");
	
	
	
	//Vokabeln hinzuf�gen
	public Panel vokabelTabelleHinzufuegen()
	{
		//Erzeuge Panel zum Hinzuf�gen von Tabellen
		pnlVokabelTabelleHinzufuegen=new Panel();

		//Layout
		//setLayout(new CardLayout());

		//Beschreibung
		Label lblVokabelTHHinweis=new Label("Hier k�nnen Sie neue Tabellen, Gegen�berstellungen, Mappings f�r den aktuellen User anlegen.");
		
		Label lblVokabelTHHinweis2=new Label("Tabelle wird nur unter aktuellem User gespeichert, wenn sie nicht schon existiert. Gro�/Kleinschreibung wird ignoriert.");
		
		Label lblVokabelTHUser=new Label("USER");

		Label lblVokabelTHBeschriftung=new Label("Tabellenname");
		
		//Textfeld f�r die neue Vokabeltabelle
		TextField txtVokabelTHName=new TextField("deutsch-english",30);


		//Button zum Speichern von der Tabelle
		Button bntVokabelTHSpeichern=new Button("Tabelle X speichern");
		bntVokabelTHSpeichern.addActionListener(rActionListener);

		//Button zum Abbrechen
		Button bntVokabelTHAbbrechen=new Button("Abbrechen");
		bntVokabelTHAbbrechen.addActionListener(rActionListener);
		
		//Adde ------------------------------
		
		pnlVokabelTabelleHinzufuegen.add(lblVokabelTHHinweis);
		
		pnlVokabelTabelleHinzufuegen.add(lblVokabelTHHinweis2);
		
		pnlVokabelTabelleHinzufuegen.add(lblVokabelTHBeschriftung);
		
		pnlVokabelTabelleHinzufuegen.add(lblVokabelTHUser);
		
		pnlVokabelTabelleHinzufuegen.add(txtVokabelTHName);

		pnlVokabelTabelleHinzufuegen.add(bntVokabelTHSpeichern);
		
		pnlVokabelTabelleHinzufuegen.add(bntVokabelTHAbbrechen);
		
		//------------------------------------
		return pnlVokabelTabelleHinzufuegen;
	}
	
	//Vokabel Satz ausw�hlen
	public Panel vokabelTabelleAuswahl()
	{
		//Erzeuge Panel zum Hinzuf�gen von Tabellen
		pnlVokabelTabelleHinzufuegen=new Panel();
		
		//Layout
		//setLayout(new BoxLayout());//Swing ....
		//setLayout(NULL);

		//Test
		//Tabelle tab=new Tabelle("deutsch-english");
		//tab.addVokabeln("deutsch","english");
		//tab.addVokabeln("Auto","car");

		//Beschreibung
		Label lblVokabelTH=new Label("Sie k�nnen neue Vokabeln, Gegen�berstellungen eingeben. Es werden nur....");//+tab
		

		//Canvas zum Testen
		Canvas canvas=new Canvas();
		
		
		
		
		//Button zum hinzuf�gen einer Vokabeltabelle zum aktuellen User
		Button bntVokabelTHSpeichern=new Button("Tabelle x unter User y speichern");
		bntVokabelTHSpeichern.addActionListener(rActionListener);
		
		//Button zum hinzuf�gen einer Vokabeltabelle zum aktuellen User
		Button bntVokabelTHAbbrechen=new Button("Tabelle x nicht f�r User y speichern");
		bntVokabelTHAbbrechen.addActionListener(rActionListener);
		
		//Adde ----------------
		//
		pnlVokabelTabelleHinzufuegen.add(lblVokabelTH);
		//
		pnlVokabelTabelleHinzufuegen.add(lblVokabelTH);
		//
		pnlVokabelTabelleHinzufuegen.add(bntVokabelTHSpeichern);
		//
		pnlVokabelTabelleHinzufuegen.add(bntVokabelTHAbbrechen);
		
		//canvas.drawRect(10,10,40,40);
		
		//Canvas
		pnlVokabelTabelleHinzufuegen.add(canvas);
		
		//System.out.println(buchstabensalat("origxnal"));
		
		//------------------------------------
		return pnlVokabelTabelleHinzufuegen;
	}
	
	//vokabelHinzufuegen
	public Panel vokabelHinzufuegen()
	{
		//Erzeugt zum Hinzuf�gen von zwei Vokabeln
		pnlVokabelHinzufuegen=new Panel();
		
		//Layout
		
		//Panel mit User einblenden

		//Beschriftung f�r das Hinzuf�gen von Vokabeln
		Label lblVokabelHVHinweis=new Label("Sie k�nnen nun zwei Vokabeln oder Gegen�berstellungen eingeben.");
		
		//Beschriftung f�r das erfolgreiche Speichern von Vokabeln
		Label lblVokabelHVHinweis2=new Label("Sie werden nur gespeichert, wenn sie noch nicht als Paar vohanden sind.");
		
		//Beschriftung f�r das Hinzuf�gen von Vokabeln
		Label lblVokabelHVLinkesWort=new Label("Linkes Wort");
		
		//Beschriftung f�r das Hinzuf�gen von Vokabeln
		Label lblVokabelHVRechtesWort=new Label("Rechtes Wort");
		
		//Textfeld f�r das linke Wort
		TextField txtVokabelHinzufuegenLinks=new TextField("Wort",30);
		
		//Textfeld f�r das rechte Wort
		TextField txtVokabelHinzufuegenRechts=new TextField("�bersetzung, Gegen�berstellung",30);
		
		//Button zum Hinzuf�gen von Vokabel-Paar
		Button bntVokabelHVSpeichern=new Button("Vokabeln speichern");
		
		bntVokabelHVSpeichern.addActionListener(rActionListener);
		
		//Button zum Abbrechen
		Button bntVokabelHVAbbrechen=new Button("Abbrechen");
		
		bntVokabelHVAbbrechen.addActionListener(rActionListener);
		
		//Adden----------------
		//Label
		pnlVokabelHinzufuegen.add(lblVokabelHVHinweis);
		//Label2
		pnlVokabelHinzufuegen.add(lblVokabelHVHinweis2);
		//Label Wort links
		pnlVokabelHinzufuegen.add(lblVokabelHVLinkesWort);
		//Label Wort rechts
		pnlVokabelHinzufuegen.add(lblVokabelHVRechtesWort);
		//Textfeld
		pnlVokabelHinzufuegen.add(txtVokabelHinzufuegenLinks);
		//Textfeld
		pnlVokabelHinzufuegen.add(txtVokabelHinzufuegenRechts);
		//Button
		pnlVokabelHinzufuegen.add(bntVokabelHVSpeichern);
		//Button
		pnlVokabelHinzufuegen.add(bntVokabelHVAbbrechen);
		
		
		//------------------------------------
		return pnlVokabelHinzufuegen;
	}

	//Textfeld f�r die neue Vokabeltabelle
	//TextField txtVokabelTHName=new TextField("deutsch-english",30);

	//Vokabel editieren
	public Panel vokabelEditieren()
	{
		//Erzeuge Panel zum Hinzuf�gen von Tabellen
		pnlVokabelEditieren=new Panel();
		
		//Layout
		//setLayout(new CardLayout());



		//Beschriftung f�r das Editieren von Vokabeln
		Label lblVokabelEditHinweis=new Label("Aus der gew�hlten Tabelle, k�nnen sie das Paar aussuchen, welches Sie �ndern wollen.");
		
		//Liste mit den Vokabel, W�rtern
		List vokabelList=new List();
		//TestTabellen
		vokabelList.add("Auto - car");
		vokabelList.add("Mann - man");
		
		//Beschriftung f�r das Sichern von editierten Vokabeln
		Label lblVokabelEditHinweis2=new Label("Es wird nur ge�ndert, wenn die Vokabel und die Gegen�berstellung nicht schon in der Tabelle ist.");
		
		//Auswahl
		
		//Textfeld f�r das linke Wort
		TextField txtVokabelEditWortLinks=new TextField("Altes Wort",30);
		
		txtVokabelEditWortLinks.setEditable(false);
		
		//Textfeld f�r die
		TextField txtVokabelEditWortRechts=new TextField("Alte Gegen�berstellung",30);
		
		txtVokabelEditWortRechts.setFocusable(false);
		
		//Button zum Hinzuf�gen von Vokabel-Paar
		Button bntVokabelEditSpeichern=new Button("Editierte Vokabeln speichern");

		bntVokabelEditSpeichern.addActionListener(rActionListener);

		//Button zum Abbrechen
		Button bntVokabelEditAbbrechen=new Button("Abbrechen");

		bntVokabelEditAbbrechen.addActionListener(rActionListener);
		
		//Adden
		
		//lblVokabelEditHinweis
		pnlVokabelEditieren.add(bntVokabelEditSpeichern);
		
		//lblVokabelEditHinweis2
		pnlVokabelEditieren.add(lblVokabelEditHinweis);
		
		pnlVokabelEditieren.add(vokabelList);

		pnlVokabelEditieren.add(lblVokabelEditHinweis2);

		pnlVokabelEditieren.add(txtVokabelEditWortLinks);

		pnlVokabelEditieren.add(txtVokabelEditWortRechts);
		
		pnlVokabelEditieren.add(bntVokabelEditSpeichern);
		
		pnlVokabelEditieren.add(bntVokabelEditAbbrechen);
		
		//------------------------------------
		return pnlVokabelEditieren;
	}
	
	//Vokabel l�schen
	public Panel vokabelnLoeschen()
	{
		//Erzeuge Panel zum Hinzuf�gen von Tabellen
		pnlVokabelLoeschen=new Panel();

		//Layout
		//setLayout(new CardLayout());

		//Beschriftung f�r das L�schen von Vokabeln
		Label lblVokabelLoeschHinweis=new Label("Aus der gew�hlten Tabelle, k�nnen sie das Paar aussuchen, welches Sie l�schen wollen.");

		//Liste f�r die Auswahl, aus der die Vokabel gel�scht werden kann
		List vokabelLoeschList=new List();
		//Testteintr�ge
		vokabelLoeschList.add("Auto - car");
		vokabelLoeschList.add("Frau - woman");
		
		
		//Button zu L�schen von einem Vokabel-Paar
		Button bntvokabelLoesch=new Button("Vokabel l�schen");
		bntvokabelLoesch.addActionListener(rActionListener);
		
		//Button zum Abrechen
		Button bntvokabelLoeschAbbrechen=new Button("Vokabel l�schen Abbrechen");
		bntvokabelLoeschAbbrechen.addActionListener(rActionListener);

		//Beschreibung
		Label lblVokabelTH=new Label("");
		
		//Adden-------------------
		
		pnlVokabelLoeschen.add(lblVokabelLoeschHinweis);
		
		pnlVokabelLoeschen.add(vokabelLoeschList);
		
		pnlVokabelLoeschen.add(bntvokabelLoesch);
		
		pnlVokabelLoeschen.add(bntvokabelLoeschAbbrechen);
		
		//------------------------------------
		return pnlVokabelLoeschen;
	}
	

	//Spielmodi-----------------------------------------
	
	public Panel spielKarteikartenModus()
	{
		//Erzeuge Panel zum Hinzuf�gen von Tabellen
		pnlSpielKarteikartenModus=new Panel();

		//Layout
		//setLayout(new CardLayout());

		//Beschreibung
		Label lblSpielKarteikartenMTabelle=new Label("Sie �ben im Karteikarten Modus mit Tabelle X. Zum �ndern bitte xyv.");
		
		//Beschreibung
		Label lblSpielKarteikartenMHiweis=new Label("Vokabel werden nach Zufallsprinzip angezeigt.");
		
		//Beschreibung
		Label lblSpielKarteikartenMFortschritt=new Label("Die Vokabel ist die xte, sie habe noch y vor sich.");
		
		//Beschreibung
		Label lblSpielKarteikartenMAuswertung=new Label("Sie haben x richtig �bersetzt, und y falsch.");
		
		//Textfeld f�r die Vorgabe
		TextField txtSpielKarteikartenMVorgabe=new TextField("Beispiel",30);
		txtSpielKarteikartenMVorgabe.setEditable(false);
		//Textfeld f�r die Vorgabe
		TextField txtSpielKarteikartenMUebersetzung=new TextField("�bersetzung",30);

		//Button zum Pr�fen des Ergebnis
		Button bntSpielKarteikartenMPruefen=new Button("�bersetzung pr�fen");
		bntSpielKarteikartenMPruefen.addActionListener(rActionListener);
		
		//Button zum Pr�fen des Ergebnis
		Button bntSpielKarteikartenMUeberspringen=new Button("Vokabel �berspringen");
		bntSpielKarteikartenMUeberspringen.addActionListener(rActionListener);
		
		
		//Adden-------------------
		
		pnlSpielKarteikartenModus.add(lblSpielKarteikartenMTabelle);
		
		pnlSpielKarteikartenModus.add(lblSpielKarteikartenMHiweis);
		
		pnlSpielKarteikartenModus.add(lblSpielKarteikartenMFortschritt);
		
		pnlSpielKarteikartenModus.add(lblSpielKarteikartenMAuswertung);
		
		pnlSpielKarteikartenModus.add(txtSpielKarteikartenMVorgabe);
		
		pnlSpielKarteikartenModus.add(txtSpielKarteikartenMUebersetzung);
		
		pnlSpielKarteikartenModus.add(bntSpielKarteikartenMPruefen);
		
		pnlSpielKarteikartenModus.add(bntSpielKarteikartenMUeberspringen);

		//------------------------------------
		return pnlSpielKarteikartenModus;
	}
	
	//zweite anlegen
	
	
	
	public Panel spielBuchstabenModus()
	{
		//Erzeuge Panel zum Hinzuf�gen von Tabellen
		pnlSpielBuchstabenModus=new Panel();

		//Layout
		//setLayout(new CardLayout());

		//Beschreibung
  		Label lblSpielBuchstabenMTabelle=new Label("Sie �ben im Buchstaben Modus mit Tabelle X. Zum �ndern bitte xyv.");
		
		//Beschreibung
		Label lblSpielKarteikartenMTabelle=new Label("Sie �ben im Karteikarten Modus mit Tabelle X. Zum �ndern bitte xyv.");

		//Beschreibung
		Label lblSpielBuchstabenMHiweis=new Label("Vokabel werden nach Zufallsprinzip angezeigt.");

		//Beschreibung
		Label lblSpielBuchstabenMFortschritt=new Label("Die Vokabel ist die xte, sie habe noch y vor sich.");

		//Beschreibung
		Label lblSpielBuchstabenMAuswertung=new Label("Sie haben x richtig �bersetzt, und y falsch.");

		//Textfeld f�r die Vorgabe
		TextField txtSpielBuchstabenMVorgabe=new TextField("Beispiel",30);
		txtSpielBuchstabenMVorgabe.setEditable(false);

		//Textfeld f�r die Vorgabe aber Durcheinander
		TextField txtSpielBuchstabenMUebersetzung=new TextField("�bersetzung",30);

		//Button zum Pr�fen des Ergebnis
		Button bntSpielBuchstabenMPruefen=new Button("�bersetzung pr�fen");
		bntSpielBuchstabenMPruefen.addActionListener(rActionListener);
		bntSpielBuchstabenMPruefen.addActionListener(controllerListener);

		//Button zum Pr�fen des Ergebnis
		Button bntSpielBuchstabenMUeberspringen=new Button("Vokabel �berspringen");
		bntSpielBuchstabenMUeberspringen.addActionListener(rActionListener);
		
		
		//Adden--------------------
		
		pnlSpielBuchstabenModus.add(lblSpielBuchstabenMTabelle);
		
		pnlSpielBuchstabenModus.add(lblSpielBuchstabenMHiweis);
		
		pnlSpielBuchstabenModus.add(lblSpielBuchstabenMFortschritt);
		
		pnlSpielBuchstabenModus.add(lblSpielBuchstabenMFortschritt);
		
		pnlSpielBuchstabenModus.add(lblSpielBuchstabenMAuswertung);
		
		pnlSpielBuchstabenModus.add(txtSpielBuchstabenMVorgabe);
		
		pnlSpielBuchstabenModus.add(txtSpielBuchstabenMUebersetzung);
		
		pnlSpielBuchstabenModus.add(bntSpielBuchstabenMPruefen);
		
		pnlSpielBuchstabenModus.add(bntSpielBuchstabenMUeberspringen);
		
		//pnlSpielBuchstabenModus.add(txtSpielBuchstabenMVorgabe);

		//------------------------------------
		return pnlSpielBuchstabenModus;
	}
	
	public Panel spielMultipleModus()
	{
		//Erzeuge Panel zum Hinzuf�gen von Tabellen
		pnlSpielMultipleModus=new Panel();

		//Layout
		//setLayout(new CardLayout());

		//Beschreibung
		Label lblSpielMultipleChMTabelle=new Label("");
		
		//Adden-------------------------------
		
		//pnlSpielMultipleModus.add(null);
		
		//------------------------------------
		return pnlSpielMultipleModus;
	}
	
	
	
	public Panel statusleiste()
	{
		//Erzeuge Panel zum Hinzuf�gen von Tabellen
		pnlStatusleiste=new Panel();
		
		
		//----------
		return pnlStatusleiste;
	}
	
	//Card
	
	
	//-----------------get/set------------------------
	
	public String getUserHinzufuegenGivenname()
	{
		if(null!=pnlUserHinzufuegen)
		{
			return ((TextField)pnlUserHinzufuegen.getComponent(2)).getText();
		}
		else
		{
			return "Vorname";
		}
	}
	
	public void setUserHinzufuegenGivenname()
	{
		((TextField)pnlUserHinzufuegen.getComponent(2)).setText("Vorname");
	}

	public String getUserHinzufuegenSurname()
	{
		if(null!=pnlUserHinzufuegen)
		{
			return ((TextField)pnlUserHinzufuegen.getComponent(4)).getText();
		}
		else
		{
			return "Nachname";
		}
	}

	public void setStatuszeile(String meldung)
	{
		System.out.println(meldung);
		//pnlStatusleiste.getComponent(0);
	}

	//------------Listener---------------------------


	class RActionListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			//
			String menuAktion=ae.getActionCommand();
			//
			switch(menuAktion)
			{
				case"Programm speichern":
					{
						//
						setStatuszeile("Programm speichern");
						validate();
						break;
					}
				case"Programm beenden":
					{
						setVisible(false);
						dispose();
						System.exit(0);
						break;
					}
				//-------------------------------
				case"User anlegen":
					{
						removeAll();
						add(userHinzufuegen());
						validate();
						break;
					}
				case"User ausw�hlen":
					{
						removeAll();
						add(userAuswahl());
						validate();
						break;
					}
				case"User l�schen":
					{
						removeAll();
						add(userLoeschen());
						validate();
						break;
					}
				//---------------------------------
				case"Tabelle anlegen":
					{
						removeAll();
						add(vokabelTabelleHinzufuegen());
						validate();
						break;
					}
				case"Tabelle ausw�hlen":
					{
						removeAll();
						add(vokabelTabelleHinzufuegen());
						validate();
						break;
					}
				case"Tabelle l�schen":
					{
						removeAll();
						add(vokabelTabelleHinzufuegen());
						validate();
						break;
					}
					
				//-----------------------------------
				case"Vokabel hinzuf�gen":
					{
						removeAll();
						add(vokabelHinzufuegen());
						validate();
						break;
					}
				case"Vokabel bearbeiten":
					{
						removeAll();
						add(vokabelEditieren());
						validate();
						break;
					}
				case"Vokabel l�schen":
					{
						removeAll();
						add(vokabelnLoeschen());
						validate();
						break;
					}
				//-------------------------------------
				case"Lern-Modus Karteikarten":
					{
						removeAll();
						add(spielKarteikartenModus());
						validate();
						break;
					}
				case"Lern-Modus JustKlick":
					{
						removeAll();
						add(spielBuchstabenModus());
						validate();
						break;
					}
				case"Lern-Modus Flip-Reverse":
					{
						removeAll();
						add(spielMultipleModus());
						setStatuszeile("Lern-Modus Flip-Reverse");
						validate();
						break;
					}
				default:
					{
						validate();
					}
			}
		}
	}

	
	class RWindowListener extends WindowAdapter
	{
		@Override
		public void windowClosing(WindowEvent we)
		{
			setVisible(false);
			dispose();
			System.exit(0);
		}
	}
	
	//------Overrides
	
	@Override
	public String toString()
	{
		return "View";
	}
}
