/*
 * R.Schneider
 * Projekt Vokabeltrainer
 * 23.02.2018
 * //git clone https://christopherpike@bitbucket.org/christopherpike/*.git
 * // git commit -m "Initial commit" git push -u origin master      https://docs.oracle.com/javase/6/docs/
 * Tabelle
 */


import java.io.*;
import java.util.*;
//package Vokabeltrainer;


class Tabelle implements Comparable,Serializable
{
	//Tabellenbezeichner
	private final String tabelleName;
	
	//TreeSet nicht sinnvoll f�r Mapping
	//private TreeSet<String>
	
	private HashMap<String,String>vokabeln;
	
	//------------------
	
	public Tabelle(String tabelleName)
	{
		this.tabelleName=tabelleName;
		vokabeln=new HashMap<String,String>();
		//richtig falsch
	}
	
	//----------------------
	
	//----get/set--------
	
	//Verf�gbare Vokabeln
	public int getSize()
	{
		return vokabeln.size();
	}
	
	//adder
	
	//
	public boolean addVokabeln(String key,String value)
	{
		if(null!=key&&null!=value)
		{
			vokabeln.put(key,value);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//---------Methoden----------
	
	//--------Overrides----------
	//hashCode
	//equals
	
	@Override
	public int compareTo(Object obj)
	{
		return 1;
	}
	
	@Override
	public String toString()
	{
		return "Tabelle "+tabelleName+" h�lt "+vokabeln.size()+" Vokabeln.";
	}
}
